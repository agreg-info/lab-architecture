let Addresses = List Text

let Nameservers =
      { Type =
        {
          addresses: Optional Addresses
        }
        , default = {
          addresses = None Addresses
        }
      }

let InterfaceConfig =
      { Type =
          {
            nameservers : Optional Nameservers.Type
          , addresses : Optional Addresses
          , dhcp4 : Optional Bool
          , dhcp6 : Optional Bool
          }
      , default = {
          nameservers = None Nameservers.Type
        , addresses = None Addresses
        , dhcp4 = None Bool
        , dhcp6 = None Bool
      }
      }

in  { Addresses
    , Nameservers
    , InterfaceConfig
    }

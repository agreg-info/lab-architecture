let configuration = ./configuration.dhall
let interfaces = ./interfaces.dhall
let utils = ./utils.dhall
let storage = ./storage.dhall

in  { autoinstall =
      { identity =
        { hostname = "localhost"
        , password = configuration.local_admin_password
        , username = configuration.local_admin_username
        }
      , proxy = "10.0.0.1:3128"
      , user-data =
        { users = 
          [
            { name = configuration.local_admin_username, passwd = configuration.local_admin_password, shell = "/bin/bash", homedir = "/var/local/localadmin", lock_passwd = False },
            { name = configuration.local_guest_username, passwd = configuration.local_guest_password, shell = "/bin/bash", homedir = "/var/local/localguest", lock_passwd = False }
          ]
          , runcmd = configuration.client_runcmd
          , bootcmd = ["modprobe -r iwlwifi", "rfkill block all"]
        }      
      , keyboard = configuration.keyboard
      , refresh-installer = configuration.refresh-installer
      , interactive-sections = configuration.interactive-sections
      , locale = configuration.locale
      , network =
        { ethernets =
          [
            { mapKey = configuration.client_wan_interface, mapValue = interfaces.InterfaceConfig::{ dhcp4 = Some True } }
          ] : utils.Map Text interfaces.InterfaceConfig.Type
          , version = 2 }
      , package_update = True
      , packages = configuration.client_packages
      , apt = configuration.client_apt
      , ssh =
        { allow-pw = "no"
        , authorized-keys = configuration.authorized-keys
        , install-server = "yes"
        }
      , storage.config
        =
        [ storage.disk
            storage.DiskAction::{
            , path = configuration.client_disk
            , preserve = False
            , grub_device = True
            , id = "disk-1"
            }
        , storage.partition
            storage.PartitionAction::{
            , device = "disk-1"
            , size = storage.SizeType.Size "1M"
            , number = 1
            , preserve = False
            , id = "partition-grub-1"
            , flag = Some "bios_grub"
            }
        , storage.partition
            storage.PartitionAction::{
            , device = "disk-1"
            , size = storage.SizeType.Size "1G"
            , number = 2
            , preserve = False
            , id = "partition-boot-1"
            }
        , storage.partition
            storage.PartitionAction::{
            , device = "disk-1"
            , size = storage.SizeType.Fill -1
            , number = 3
            , preserve = False
            , id = "partition-system-1"
            }
        , storage.format
            storage.FormatAction::{
            , fstype = "ext4"
            , volume = "partition-system-1"
            , preserve = False
            , id = "format-system"
            }
        , storage.format
            storage.FormatAction::{
            , fstype = "ext4"
            , volume = "partition-boot-1"
            , preserve = False
            , id = "format-boot"
            }
        , storage.mount
            storage.MountAction::{
            , device = "format-system"
            , path = "/"
            , id = "mount-system"
            }
        , storage.mount
            storage.MountAction::{
            , device = "format-boot"
            , path = "/boot"
            , id = "mount-boot"
            , options = Some "errors=remount-ro"
            }
        ]
      , version = 1
      }
    }

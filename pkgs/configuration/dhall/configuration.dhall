let utils = ./utils.dhall
let domain = "agreg-info.org"
let ipa_domain = "ipa.${domain}"
let clients_domain = "clients.${domain}"
let ipa_ip_address = "10.0.1.1"
let ipa_cidr = "24"
let virtualization = False

in  { authorized-keys =
      [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDwZm6kUsK/FrgJPzFi5AgvqIIYfy5S/Se4/rZgB4Edx remy@typhoon"
        , "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC4lqBmCNinb8GJ9r1O1OV9BCyYYm6RW2hSqZH0xEzP0OwuvVAJd8spgervuaBA00NfH8guG3VYkXG5XUQXH4sao8q1YnNi/os06nSvKrjGkO5RdjnhDDloKBhdBcnCJtoQ+4omWHe2pEzlgaph3M/4wT9zr+AkE6vQ44GZAkNBUevaEDPYvqC6eI98ukeDwjPDQ72KnqqP1asYr+sAKXy1KHEMklK0C63aBGUBrcJIndTr28xb4iFpfG299ECPstbPmpnJwHO6ys8JWqcLwxoEn7oJaDw9dF94U4RYsJXMs+HxFtSAvJxmvKoG53gRT5L0meX90qO/QGCScUbvOliJ mc@jupiter"
        , "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBj3bZEcuhIY7JFJibfAspS9KSjU0LTLN+cGVAjKSwf/rhYDi7aQ/huOV0zk9DZPJHGaUXO7bA/1HMYEl/I8ZivwRfEwVz4dqI17xMETzmzYmfcyYzua1FXs6KuxqWYvfFjf8yNN6/tpEhqxBBuhSK/Mzx87SVOONHunwJO34wARt0BtmBoGR2KeM2tHo8OLSHo/8WDj3ie28YjT9cd3lHcTH2sJ3TiP/vs8MGq80/sgVVSKNQ094mfqX7MX/2vugdUgb/7/RMRhmfrmjDe6oUb553Y+D7dJToYbiOm4NZh0soX/sUHt/Gt51hvM5SdaY96lXeuD+ZqhJBT6eZpX7t jj@pikachu"
      ]
    , virtualization = virtualization
    , remote_admin_username = "admin"
    , remote_admin_password = "https://www.youtube.com/watch?v=oHg5SJYRHA0"
    , ipa_ds_password = "https://www.youtube.com/watch?v=oHg5SJYRHA0"
    , local_admin_username = "localadmin"
    , local_admin_password =
        "$6$YDk8cPAzgkV2lp5j$stO2OnB.jpfNlBb7d42LFBWOeYgRrf0Cz3v4N9zar53lLvOljP3wVjMqkEx8sGZqKifuAb4XWbIDz8ww8jZqp."
    , local_guest_username = "localguest"
    , local_guest_password =
        "\$y\$j9T\$sVBmpF9ore0Iw.E2.4ttp0\$6RS.dsyi7MlSUVYnKB0RLaCT/tTiQMRqm.YmlhLdM4." {- localguest -}
    , ipa_domain
    , ipa_realm = utils.upperASCII ipa_domain
    , dhcp_clients_zone = clients_domain
    , dhcp_range_cidr = "10.0.1.0/${ipa_cidr}"
    , dns_server = "8.8.8.8"
    , dhcp_clients_range_cidr = "10.0.1.128/25"
    , dhcp_clients_reverse_zone = "10.in-addr.arpa."
    , ipa_ip_address
    , ipa_ip_address_plus_cidr = "${ipa_ip_address}/${ipa_cidr}"
    , server_lan_interface = if virtualization then "ens4" else "enp4s0f1"
    , server_wan_interface = if virtualization then "ens3" else "enp4s0f0"
    , client_wan_interface = if virtualization then "ens3" else "enp1s0f1"
    , server_disk = if virtualization then "/dev/vda" else "/dev/sda"
    , client_disk = if virtualization then "/dev/vda" else "/dev/nvme0n1"
    , delay = 60
    , retries = 3
    , locale = "fr_FR"
    , keyboard = { layout = "fr" }
    , refresh-installer = { update = True }
    , interactive-sections = [ "storage" ] {- due to subiquity bug -}
    , package_update = True
    , client_apt.sources.`vscodium.list`
      =
      { key =
          ''
          -----BEGIN PGP PUBLIC KEY BLOCK-----

          mQINBFu8c8sBEACrPcX4UB0rGFObGzIpa3fqJy0K//W5XENNBrGlk6DS68SwkSza
          QFoD+ZoGiwGZWy+X5m2bm6A0j8ff61uKLZFx4turikggdqey6RL3NIQOmhzbz9jc
          90ZtVG3W2VHwP1Oy2sITJBD1UdCtnRR7ONfoLVJbavPrjHL0VEJqV6kF7ki45DAK
          yVbEGjsg668l8FmfMIYyoqV8HQ+JosXQ6ItOH2QlGwvD5RvBmIRzIXSS159+UHBj
          jrk81Pb1RLWUafoh8Geh4VZHQrvqkLmds/74KZQg47zDr1atKeIRe/p7UPZIEJsr
          puOQP+4yfW8CKN/du62mO37xyJBcQpdsk+27/AAbpluwwzfrRAZcI9wqlmE7CRie
          tARI3eLA/BVeLm0U5Jxemc1asTAptQTij9BvzFfZcpkwK+AcVmaicQhJ9iaa8bQ2
          oTTE5keewHgVdun+XfeMPPuRcF5QxeVK9dZQVI3nSObOXrehulkYXusMQ4vq8bh7
          GktI5n1O2qehnSn/Is7kdLBWDy8xGlgYEWe9oStQ5NSXo4PR7YWDzPs5aXBz9LOa
          zthJqM1Ah41q5/rSgfhM3e+vXtF3M8phLoJTE5mlb370XVH1ZLNbXVvIuHO0L+Lh
          ruZc/tREFWThOiS+FIGv/MDs4DieWEwr9+/598pFERo7cJEo5AZNgPy8SQARAQAB
          tCRQYXZsbyBSdWR5aSA8cGF1bGNhcnJvdHlAcmlzZXVwLm5ldD6JAk4EEwEIADgW
          IQQTAt5gIxiJ/h66ytxUZ4z3WieNnAUCW7xzywIbAwULCQgHAgYVCgkICwIEFgID
          AQIeAQIXgAAKCRBUZ4z3WieNnPDyD/oCSpzl4uua0r/T4cL0rEhHVg82HounlTs2
          BxI0ZhJpjdqIE2ytIFiwdV47yAkt04ow0zEBHbZQldvixfsM3tDYrC6iiC4pRMMc
          W6oVGf18r8nKKRmYTaOSPWL5yhLgO/IkQ/kCBsu3Wf0bp3GbHQ4wny+rmP745oG9
          npGPPW7EiUcFYPIM/YX2YqSH/FOGcMOwlp1QEvVqypQqaogyUmDRP+6bpKYMuq/c
          GuPxHuwPmS7a3Zd6ybAtYBNumC/lNYbfxAZA1NK4WViVGB/P5GBWC0HSfLPsBu7D
          /GqoqYCDTYYtSpnw2kUpkGWqOaWZc9S5Jvp26Hw+VoVGBdJUe7s7qJyeNmdjM/cR
          /dNY9+fW3w9zWgJZcXTnScmpi0vzA7BLmFfsphPBZ7J1Sc+N72uV/W9A70yyNhSw
          DQ20/4D2AHNozmq881LhBlOIw24jb0LlbrA7CFoR10zkpXsS/Vh1EWReV1z4zJDv
          H1SKUzXaOJUpqqW0EpblEpH4qg5hknnnW4XjvlWZO/ICkKTi0LxXK9Lmcbhtzg9t
          Wh7bfDXHXoQYS4QooJbzUhAHwXvQp03R0qu6UhEVhO42y5PVQ/+18FlhgBd+zP1Q
          7Urb93f+7YbSa8e34ANcVvJZc7gP2oRTuVyKVjO/Q9l6+Qzg9DTFVPGYLvYJKIpK
          odUkTbb/ArkCDQRbvHPLARAA3zMxF1XX4tLaz/0U7p381AXmtMA2L63mRQ3YGZxg
          fVxyVx6FdLujxJHytIGnLb9FYQZkxjYyMVc8/7ukrPUTHDUHm2ab4mG0SvhDI3nx
          2qeXE2dYMkpLoBqvFLekFAU129w9BLm9lbHfN+JbbdMmoqlyiufuPM6gz0gBV6ce
          oXUtu8Q2/ixshSfvcdHx21ZD2HuNqSyworbzkA+0B0F51QRp1tqYJ7wQm3n82rQ+
          YFS97Un+7VWgJrX4aofUxRiDx0VHIYkEN8QcDZTQywT4zkj6tDMyKEp40axvZ6zM
          AlaTI1GVrGMUHH7bnZXiF2ZyKHOF6XjTuEfotHXm8fzEHLdhtdUxiQ4+GK5ajQN2
          ay/v5JEtsc3FSAmjfTW9r6jEAiDn5TbeKHtLpeGmtPqvgK/Nzo5rRRx0NgymymeF
          q7Ir4ATu9x8KKYXa/tFd2Qe8rtxNqhYjUh+W7FpMMNXYa18G+PbwNqpEGMdp51rS
          9/Lq2GSYcuCV/jD2QbySUX5wHe9zDNiOuovBFhJZS8H2OGl6aep6zEpV/Q7FcWEG
          9fz4RHADSGB6RFFI8hyV0/YaVU9rB4fAOZzybuJ4DSadOAsVVfkk2WdnUM6x1z24
          XhRdakH+ekGDg+nqs8wUc4u4ouo7dPTPaxbrUas5gdzjbtwoYXoKaIUnisPRranz
          EjsAEQEAAYkCNgQYAQgAIBYhBBMC3mAjGIn+HrrK3FRnjPdaJ42cBQJbvHPLAhsM
          AAoJEFRnjPdaJ42cEq8QAKJ1YUzQsgzUWSzkPgSZJwOAujWkDdhw+YbNvjHAgRZA
          DrNbIyIYkJ/IevubOnteJgwGP/6qaMJwemM+VJ3e/YHvcvAmIilmH33tnnMRqWsj
          xqG021SuX3FGdMXR7WwINZaToh+Lqaj4YXpanjPzGMGKZlzaSdj6avm7KT3HGHjO
          Gk/nz0rAsqXfuzQBkHZ+JozUMqrh30+POcvkyFhqmTQ9juECFzOtegbEWYdDFBMA
          tig1whBccebZ0W+Sva+e1AKktwHNtweaofCg8zQ39Nx1KOz9FoH4B2QF+dYU/yMQ
          BpPYTRQPfbaf2/t5JJaoplWrE2gP+JS8ET11J7U4xtzqs7QprcCurTZv42PjwPKu
          Hc8uwE1VaeX+d7zRGXD9ooR+u+BWKiwxD9p54/QHu/qh/x/4evrETTIZGsheojgm
          srMYT2mW1hLFcOANAgvxuIm+O51+XYviQCGPqg8J2jm5zmxMNwEhVx0ObYcnJqve
          klJniYNw9Ja9gnXxXWycChhmGM/QFLE0yrN7di7oDeN/JUhPPgppCken/xFihoxz
          hlNM8vyJMQiddIBiEcz0zcAbbT74+qFJv9KRsfWAU0MEy3a66H/hybEJu140qTNw
          d7e/dbqns8bDX1BP/x5/QKfxbZNoH2QWmCdv/b5dJ97OXFBtPM/VnOYaaFG5y2gX
          =LCWI
          -----END PGP PUBLIC KEY BLOCK-----
          ''
      , source = "deb https://download.vscodium.com/debs vscodium main"
      }
    , client_bootcmd =
      [ "apt remove -y snapd unattended-upgrades"
      , "mkdir -p /etc/ssl/certs/java/cacerts"
      ]
    , client_packages =
      [ "gnome"
      , "lxde"
      , "ocaml"
      , "jupyter"
      , "inkscape"
      , "gimp"
      , "apache2"
      , "wget"
      , "htop"
      , "language-pack-fr"
      , "vim"
      , "emacs"
      , "pyzo"
      , "zeal"
      , "nodejs"
      , "libreoffice"
      , "mariadb-server"
      , "phpmyadmin"
      , "sqlite3"
      , "git"
      , "gcc-10"
      , "build-essential"
      , "gdb"
      , "valgrind"
      , "gfortran"
      , "gprolog"
      , "openjdk-11-jdk"
      , "texlive-latex-base"
      , "texlive-lang-french"
      , "texmaker"
      , "qgis"
      , "python3-pip"
      , "r-base"
      , "graphviz"
      , "gnuplot"
      , "flex"
      , "ml-yacc"
      , "ragel"
      , "menhir"
      , "pandoc"
      , "libreoffice"
      , "libreoffice-l10n-fr"
      , "gnome-terminal"
      , "python-is-python3"
      , "firefox"
      , "chromium-browser"
      , "pyzo"
      , "subversion"
      , "codium"
      , "opam"
      , "zlib1g-dev"
      , "libffi-dev"
      , "libgmp-dev"
      , "libzmq5-dev"
      , "evince"
      , "mlocate"
      , "nemo"
      , "rfkill"
      , "rdiff-backup"
      , "lsyncd"
      , "krb5-user" {- client -}
      , "libpam-krb5"
      , "freeipa-client"
      , "nfs-common" 
      , "autofs"
      , "chrony"
      ]
    , client_runcmd =
      [ "rm /var/lib/dpkg/info/ca-certificates-java.postinst && apt install -f"
      , "git clone https://gitlab.com/agreg-info/lab-architecture.git"
      , "cd lab-architecture && bash ./scripts/install-machine.sh"
      , "cp /var/local/localadmin/.ssh/authorized_keys /root/.ssh/authorized_keys"
      ]
    }

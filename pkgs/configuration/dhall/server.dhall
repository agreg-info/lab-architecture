let configuration = ./configuration.dhall
let storage = ./storage.dhall
let utils = ./utils.dhall
let interfaces = ./interfaces.dhall

in  { autoinstall =
      { identity =
        { hostname = configuration.ipa_domain
        , password = configuration.local_admin_password
        , username = configuration.local_admin_username
        }
      , keyboard = configuration.keyboard
      , locale = configuration.locale
      , network =
        { ethernets =
          [
              { mapKey = configuration.server_lan_interface, mapValue = interfaces.InterfaceConfig::{ dhcp4 = Some False, dhcp6 = Some False, addresses = Some [configuration.ipa_ip_address_plus_cidr ], nameservers = Some { addresses = Some [ configuration.ipa_ip_address ] } } }
            , { mapKey = configuration.server_wan_interface, mapValue = interfaces.InterfaceConfig::{ dhcp4 = Some True } }
          ] : utils.Map Text interfaces.InterfaceConfig.Type
        , version = 2
        }
      , package_update = True
      , packages = [ "console-data" ]
      , ssh =
        { allow-pw = "no"
        , authorized-keys = configuration.authorized-keys
        , install-server = "yes"
        }
      , storage.config
        =
        [ storage.disk
            storage.DiskAction::{
            , path = configuration.server_disk
            , preserve = False
            , grub_device = True
            , id = "disk-1"
            }
        , storage.partition
            storage.PartitionAction::{
            , device = "disk-1"
            , size = storage.SizeType.Size "1M"
            , number = 1
            , preserve = False
            , id = "partition-grub-1"
            , flag = Some "bios_grub"
            }
        , storage.partition
            storage.PartitionAction::{
            , device = "disk-1"
            , size = storage.SizeType.Size "1G"
            , number = 2
            , preserve = False
            , id = "partition-boot-1"
            }
        , storage.partition
            storage.PartitionAction::{
            , device = "disk-1"
            , size = storage.SizeType.Fill -1
            , number = 3
            , preserve = False
            , id = "partition-system-1"
            }
        , storage.format
            storage.FormatAction::{
            , fstype = "ext4"
            , volume = "partition-system-1"
            , preserve = False
            , id = "format-system"
            }
        , storage.format
            storage.FormatAction::{
            , fstype = "ext4"
            , volume = "partition-boot-1"
            , preserve = False
            , id = "format-boot"
            }
        , storage.mount
            storage.MountAction::{
            , device = "format-system"
            , path = "/"
            , id = "mount-system"
            }
        , storage.mount
            storage.MountAction::{
            , device = "format-boot"
            , path = "/boot"
            , id = "mount-boot"
            , options = Some "errors=remount-ro"
            }
        ]
      , version = 1
      }
    }

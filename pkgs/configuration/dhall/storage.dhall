let Ptable = < msdos | gpt | vtoc >

let StorageType = < disk | partition | format | mount >

let WipeType = < superblock | superblock-recursive | pvremove | zero | random >

let SizeType = < Size : Text | Fill : Integer >

let DiskAction =
      { Type =
          { type : Text
          , ptable : Ptable
          , path : Text
          , wipe : WipeType
          , preserve : Bool
          , grub_device : Bool
          , id : Text
          }
      , default =
        { type = "disk"
        , ptable = Ptable.gpt
        , preserve = True
        , grub_device = False
        , wipe = WipeType.superblock
        }
      }

let PartitionAction =
      { Type =
          { type : Text
          , device : Text
          , size : SizeType
          , wipe : WipeType
          , number : Natural
          , preserve : Bool
          , flag: Optional Text
          , id : Text
          }
      , default =
        { type = "partition", wipe = WipeType.superblock, preserve = True, flag = None Text }
      }

let FormatAction =
      { Type =
          { type : Text
          , fstype : Text
          , volume : Text
          , preserve : Bool
          , id : Text
          }
      , default = { type = "format", preserve = True }
      }

let MountAction =
      { Type =
          { type : Text
          , device : Text
          , path : Text
          , id : Text
          , options : Optional Text
          }
      , default = { type = "mount", options = None Text }
      }

let StorageType
    : Type
    = < disk : DiskAction.Type
      | partition : PartitionAction.Type
      | format : FormatAction.Type
      | mount : MountAction.Type
      >

in  { StorageType
    , DiskAction
    , PartitionAction
    , FormatAction
    , MountAction
    , SizeType
    , disk = StorageType.disk
    , partition = StorageType.partition
    , format = StorageType.format
    , mount = StorageType.mount
    }

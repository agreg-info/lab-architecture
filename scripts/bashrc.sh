# Append to /etc/skel/.bashrc
export OPAMROOT=/usr/local/opam
eval $(opam env)

# lsync $HOME to /home/localguest
# sed -i -e "/source =/ s|= .*|= '$HOME',|" $HOME/.config/lsyncd/lsyncd.conf.lua
# lsyncd $HOME/.config/lsyncd/lsyncd.conf.lua

# Symbolic links
cd /var/www/html && ln -s /usr/local/share/Zeal/docsets zeal

# If small LaTeX
export PATH=$HOME/.local/bin:/usr/local/share/texlive/bin/x86_64-linux:$PATH

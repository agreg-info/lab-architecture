(echo "# Backups every 5 min"; echo "*/5 * * * * NOW=\$(date +'\%d-\%m-\%Y-\%H:\%M:\%S'); rdiff-backup --exclude='**/.*' $HOME /var/local/backups/backup-\$NOW") | crontab -

# Tell Codium where to find extensions
export VSCODE_EXTENSIONS=/usr/local/share/codium

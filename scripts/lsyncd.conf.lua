settings {
    logfile = "/var/local/backups/lsyncd/lsyncd.log",
    statusFile = "/var/local/backups/lsyncd/lsyncd.status",
    statusInterval = 20,
    nodaemon = false   
}

sync {
    default.rsync,
    source = "<will be replaced with absolute path to $HOME by .bashrc>",
    target = "/var/local/backups/latest/"
}

# Enable phpmymadmin
# ln -s /usr/share/phpmyadmin/ /var/www/html
# mysql -e "create user candidat identified by 'concours'"
# mysql -e "grant all privileges on *.* to candidat with grant option"
# mysql -e "flush privileges"

# Load docs
git clone https://gitlab.com/agreg-info/docs /var/www/html/docs
# cd /var/www/html/docs && pandoc index.md -o index.html

# snap PyCharm and Chromium
snap install pycharm-community --classic
# snap install chromium

# Locale
localectl set-locale fr_FR.UTF-8
localectl set-x11-keymap fr

# Cleanup
apt autoclean

# Python packages
python3 -m pip install -r scripts/requirements.txt
pip freeze -r scripts/requirements.txt > scripts/installed-packages.txt

# OCaml packages installed globally
export OPAMROOT=/usr/local/opam
opam init -n && opam install -y jupyter utop ocaml-lsp-server && eval $(opam env) && ocaml-jupyter-opam-genspec
mkdir -p /usr/local/share/jupyter
jupyter kernelspec install --name ocaml-jupyter "$(opam var share)/jupyter"

# Zeal global config
mkdir -p /usr/local/share/Zeal/docsets
sed -i -e '/path=/ s/=.*/=\/usr\/local\/share\/Zeal\/docsets/' /etc/skel/.config/Zeal/Zeal.conf

# Backup folder
mkdir -p /var/local/backups
chmod o+w /var/local/backups
mkdir -p /var/log/lsyncd
chmod o+w /var/log/lsyncd
chmod o+w /var/local/localguest

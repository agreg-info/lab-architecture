#!/usr/bin/python

import ipaddress

class FilterModule(object):
    def filters(self):
        return {
            'ip_in_range': self.ip_in_range,
            'network_address': self.network_address,
            'netmask': self.netmask,
            'reverse_ip': self.reverse_ip
        }

    def ip_in_range(self, range):
        network = ipaddress.IPv4Network(range)
        l = [str(ip) for ip in network]
        return l[1:-1] # Ignore the network and broadcast addresses

    def network_address(self, range):
        network = ipaddress.IPv4Network(range)
        return str(network.network_address)

    def netmask(self, range):
        network = ipaddress.IPv4Network(range)
        return str(network.netmask)

    def reverse_ip(self, ip):
        ip = ipaddress.ip_address(ip)
        reverse_octets = str(ip).split('.')[::-1]
        return '.'.join(reverse_octets)
import os
import pandas as pd
from string import Template


with open('article.tex') as f:
    template = Template(f.read())

df = pd.read_csv('full-logins2024.csv')

for i, row in df.iterrows():
    filename = f"{i:03d}-{row['login']}.tex"
    with open(f"docs/{filename}", 'w') as f:
        f.write(template.substitute(username=row['username'], password=row['password']))
    print(filename)

import pandas as pd


with open('names2.txt') as f:
	for line in f.read().splitlines():
		elements = line.split()
		first_name = elements[0]
		last_name = elements[1]

		for challenge in ['tp', 'model', 'lec']:
			username = f"{last_name.lower()}-{challenge}"
			# pwgen -AB0 8 1 | tee
			print(f"cat ~/passwords/{username} | ipa user-add {username} --first {first_name} --last {last_name} --shell /bin/bash --password &")

# echo "super" | ipa user-add dassin --first Joe --last Dassin --shell /bin/bash --password
# echo "coucou" | ipa user-add smith --first John --last Smith --shell /bin/bash --password
# echo "wow" | ipa user-add doe --first John --last Doe --shell /bin/bash --password
# Penser à su dans admin
# echo "coincoin" | ipa user-add coincoin --first Coin --last Coin --shell /bin/bash --password
# echo "christie" | ipa user-add christie --first Christie --last Agatha --shell /bin/bash --password
echo "cuncun" | ipa user-add cuncun --first Cun --last Cun --shell /bin/bash --password
echo "cancun" | ipa user-add cancun --first Can --last Cun --shell /bin/bash --password
echo "jj" | ipa user-add jj --first J --last J --shell /bin/bash --password

echo "turing" | ipa user-add turing --first Alan --last Turing --shell /bin/bash --password
su turing

kinit -P admin
echo "bellman" | ipa user-add bellman --first Richard --last Bellman --shell /bin/bash --password
echo "lovelace" | ipa user-add lovelace --first Ada --last Lovelace --shell /bin/bash --password

echo "pythagore" | ipa user-add pythagore --first Théorème --last Pythagore --shell /bin/bash --password

echo "dijkstra" | ipa user-add dijkstra --first Edsger --last Dijkstra --shell /bin/bash --password && su dijkstra && logout

echo "papadimitriou" | ipa user-add papadimitriou --first Christos --last Papadimitriou --shell /bin/bash --password && su papadimitriou && logout
